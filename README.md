This is the solution to some of the tasks asked in
https://github.com/tillpayments/till-dev-test

total time spent: strictly 4 hours.
Backend: 1/2 hour in studying typesript and 1 hours in developement and testing
frond end: 1 hour 30 mins in studying and trying the various techstacks and 1 hour in development and testing

The project contains 2 submodules till-task (backend) and my-app (front end)
both executes on seprarate node. back end on 3001 and front end on 3000.

backend is witten in typescript and front end in react
