import React, { Component } from 'react';
import ReactDOM from 'react-dom';
//import logo from './logo.svg';
import './App.css';

class App extends Component {
  
  state = {
    customers: [],
    merchants: []
  }
  componentDidMount () {
    fetch('http://localhost:3001/api/customers')
      .then(response => response.json())
      .then(response => { 
        //set the customers in state
        this.setState({customers:response})
      });
      fetch('http://localhost:3001/api/merchants')
      .then(response => response.json())
      .then(response => { 
        //set the merchants in state
        this.setState({merchants:response})
      });
      
  }
  trans(m){
    //alert(m);
    //console.log(m);
    const facts = 
      
      <div>
        <center>
          <h3>Transactions</h3>
        </center>
        
        <table className="table table-hover table-bordered table-striped">
        <thead>
            <tr>
              <th>Id</th>
              <th>amount</th>
              <th>description</th>
              <th>ccLastFour</th>
              <th>ccExpiry</th>
              <th>ccToken</th>
              <th>customerId</th>
              <th>date</th>
            </tr>
          </thead>
          <tbody>
          {m.transactions.map(c =>(
                  <tr key={c.id}>
                    <td>{c.id}</td>
                    <td>{c.amount/100}{m.currency}</td>
                    <td>{c.description}</td>
                    <td>{c.ccLastFour}</td>
                    <td>{c.ccExpiry}</td>
                    <td>{c.ccToken}</td>
                    <td>{c.customerId}</td>
                    <td>{c.date}</td>
                  </tr>
          ))
              }
          </tbody>
        </table>
        </div>
     ReactDOM.render(facts, document.getElementById("sample"));
  }

  render () {
    return (
      
      <div>
        <center>
          <h3>Customers List</h3>
        </center>
        
        <table className="table table-hover table-bordered table-striped">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>MerchantId</th>
            </tr>
          </thead>
          <tbody>
          {this.state.customers.map(c =>(
                  <tr key={c.id}>
                    <td>{c.id}</td>
                    <td>{c.name}</td>
                    <td>{c.merchantId}</td>
                  </tr>
          ))
              }
          </tbody>
        </table>
        <center>
          <h3>Merchants List </h3>(click on merchant to load transaction)
        </center>
        
        <table className="table table-hover table-bordered table-striped">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>isTrading</th>
              <th>Currency</th>
            </tr>
          </thead>
          <tbody>
          {this.state.merchants.map(m =>(
                  <tr className="table-row" key={m.id} onClick={() => this.trans(m)}>
                    <td>{m.id}</td>
                    <td>{m.name}</td>
                    <td>{m.isTrading.toString()}</td>
                    <td>{m.currency}</td>
                  </tr>
          ))
              }
          </tbody>
        </table>
        <div id="sample" ></div>
      </div>)
  }
}
  export default App;
