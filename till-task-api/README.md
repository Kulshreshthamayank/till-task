## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

pre requirements, node must be installed

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

steps:
npm i -g @nestjs/cli

create a nest project
nest new till-task

use npm as manager when asked


once its setup, kindly replace src from this module to your project

once done.

npm run start:dev

this will expose below end points:

localhost:3001/api/merchants, GET

localhost:3001/api/merchants/:id, GET

localhost:3001/api/customers, GET

localhost:3001/api/customers/:id, GET

localhost:3001/api/customers, POST

{
    "id": "019",
    "merchantId": "001",
    "name": "steve moodsy"
  }

localhost:3001/api/customers, PUT

{
    "id": "019",
    "merchantId": "001",
    "name": "steve moodsy"
  }

localhost:3001/api/customers/:id, DELETE

validations added

---------------------
response:
{
    "statusCode": 404,
    "message": "Cannot PUT /api/customers/021",
    "error": "Not Found"
}

---------------------
