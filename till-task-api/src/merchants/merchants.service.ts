import { Inject, Injectable } from '@nestjs/common';
import { Data } from 'src/data';

@Injectable()
export class MerchantsService {
    
    @Inject(Data)
    private data;
    
      getMerchants(){
          return this.data.merchants;
      }
      
}
