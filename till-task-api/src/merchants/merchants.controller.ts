import { Controller, Get, Param } from '@nestjs/common';
import { MerchantsService } from './merchants.service';

@Controller('api/merchants')
export class MerchantsController {
    constructor(private ms:MerchantsService){}

    @Get()
    getMerchants(){
        return this.ms.getMerchants();
    }

    @Get(':id')
    getMerchant(@Param() params){
        return this.ms.getMerchants().filter(i => i.id==params.id);
    }
}
