import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { CustomersService } from './customers.service';

interface CustomerDto {
    id: string;
    merchantId:string;
    name: string;
  }

@Controller('api/customers')
export class CustomersController {
    constructor(private customerService:CustomersService){}

    @Get()
    getCustomers(){
        console.log("get all customers")
        return this.customerService.getCustomers();
    }

    @Get(':id')
    getCustomer(@Param() params){
        console.log("get customer",params.id)
        return this.customerService.getCustomers().filter(item => item.id==params.id);
    }

    @Post()
    createCustomer(@Body() customer:CustomerDto){
        console.log("create ustomer",customer)
        this.customerService.createCustomer(customer);
    }

    @Put()
    updateCustomer(@Body() customer:CustomerDto){
        console.log("update ustomer",customer)
        this.customerService.updateCustomer(customer);
    }

    @Delete(':id')
    deleteCustomer(@Param() params){
        console.log("delete ustomer",params.id)
        this.customerService.deleteCustomer(params.id);
    }
}
