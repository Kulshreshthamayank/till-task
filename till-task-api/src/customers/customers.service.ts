import { BadRequestException, Injectable } from '@nestjs/common';
import { Data } from 'src/data';

@Injectable()
export class CustomersService {
  
  constructor(private data:Data){

  }
    getCustomers(){
      return this.data.customers;
        /*var cutomrestoreturn = [];
        var ts = this.data.merchants.map(t => t.transactions);
        //console.log("item found",ts);
        const result = ts.reduce((accumulator, value) => accumulator.concat(value), []);
        //console.log("item found",result);
       var ms = this.data.merchants;
        this.data.customers.forEach(function(value){
          var transactions = result.filter(item => item.id==value.id);
          //console.log(transactions);
          var objectC = {...value, transactions};
          cutomrestoreturn.push(objectC);
        });
        return cutomrestoreturn;
        */
    }
    createCustomer(customer){
      var merchantIds = this.data.merchants.map(item=>item.id);
      console.log("existing merchants",merchantIds);
      if(merchantIds.indexOf(customer.merchantId)==-1){
        console.log("merchant doesnt exist",customer.merchantId);
        throw new BadRequestException('invalid merchantId');
      }
      this.data.customers.push(customer);
    }

    deleteCustomer(id){
      this.data.customers = this.data.customers.filter(item => item.id!=id);
    }

    updateCustomer(customer){
      var c = this.data.customers.filter(item => item.id==customer.id);
      console.log("item found",c);
      if(Array.isArray(c) && c.length==0){
        console.log("id not found",customer.id);
        throw new BadRequestException('customer not found');
      }
      var merchantIds = this.data.merchants.map(item=>item.id);
      console.log("existing merchants",merchantIds);
      if(merchantIds.indexOf(customer.merchantId)==-1){
        console.log("merchant doesnt exist",customer.merchantId);
        throw new BadRequestException('invalid merchantId');
      }
      this.data.customers = this.data.customers.filter(item => item.id!=customer.id);
      this.data.customers.push(customer);
    }
}
